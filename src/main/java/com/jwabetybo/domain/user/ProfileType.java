package com.jwabetybo.domain.user;

public enum ProfileType {

    INSPECTOR ("Inspecteur"),
    DIRECTOR ("Directeur "),
    ADMINISTRATOR ("Administrateur"),
    ADVISOR ("Conseiller"),
    SUPERVISOR ("superviseur"),
    HYPERVISOR ("hyperviseur");

    private String profileType = "";
    //Constructeur
    ProfileType(String profileType){
        this.profileType = profileType;
    }
    public String toString(){
        return profileType;
    }
}