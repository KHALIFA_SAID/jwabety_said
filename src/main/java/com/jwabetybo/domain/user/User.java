package com.jwabetybo.domain.user;

import com.jwabetybo.domain.network.Network;
import com.jwabetybo.domain.organization.Organization;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String brokerage_code;
    private String email;
    @Column(name="first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    private String login;
    private String password;
    private Long status;
    private ProfileType profil;
    @Column(name = "url_avatar")
    private String urlAvatar;
    private Long civility;
    @Column(name="birth_date")
    private Date birthDate;
    private String phone;
    private String address;
    @Column(name = "postal_code")
    private String postalCode;
    @CreationTimestamp
    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt;
    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
    @OneToMany(mappedBy = "user")
    private List<Network> network;
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organisation;



}

