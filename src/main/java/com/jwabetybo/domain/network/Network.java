package com.jwabetybo.domain.network;

import com.jwabetybo.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Network {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="superior_hiearchy_Id")
    private Long superiorHiearchyId;
    @Column(name="start_date")
    private Date startDate;
    @Column(name="type_network")
    private Long typeNetwork;
    @Column(name="created_at")
    @UpdateTimestamp
    private LocalDateTime createAt;
    @Column(name="updated_at")
    @UpdateTimestamp
    private LocalDateTime updateAt;
    @ManyToOne @JoinColumn(name = "user_id")
    private User user;
}
