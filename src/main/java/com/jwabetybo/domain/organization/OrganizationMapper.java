package com.jwabetybo.domain.organization;



import com.jwabetybo.dto.OrganizationRequest;
import com.jwabetybo.dto.OrganizationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;

@Slf4j
@Service
public class OrganizationMapper {

    private final OrganizationRepository organizationRepository;

    public OrganizationMapper(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }


    OrganizationResponse toDto(Organization organization){
        return OrganizationResponse.builder()
                .mail(organization.getEmail())
                .name(organization.getName())
                .phone(organization.getPhone())
                .website(organization.getWebsite())
                .createdAt(organization.getCreatedAt().atOffset(ZoneOffset.UTC))
                .addressId(organization.getAddress().getId())
                .build();
    }

    Organization toEntity(OrganizationRequest organizationRequest){
        return Organization.builder()
                .originOrganization(organizationRepository.findById(organizationRequest.getAgencyId()).get()).build();
                //.description(organizationRequest.ge)
                //.address(organizationRequest.addressId())
                //.agency(organizationRepository.findById())
    }


}
