package com.jwabetybo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwabetyboApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwabetyboApplication.class, args);
	}

}
