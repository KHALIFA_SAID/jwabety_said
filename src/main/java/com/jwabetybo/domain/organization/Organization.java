package com.jwabetybo.domain.organization;

import com.jwabetybo.domain.address.Address;
import com.jwabetybo.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JoinColumn(name = "organisation_id")
    private Long id;
    private String name;
    private String description;
    @JoinColumn(name = "logo_id")
    private Long logoId;
    private String website;
    private String phone;
    private String email;
    @OneToMany(mappedBy = "organisation")
    private List<User> users;
    @ManyToOne
    @JoinColumn(name = "origin_organization_id")
    private Organization originOrganization;
    @CreationTimestamp
    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt;
    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
    @ManyToOne
    private Address address;
}
