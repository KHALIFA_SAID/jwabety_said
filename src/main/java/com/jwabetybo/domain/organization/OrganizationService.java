package com.jwabetybo.domain.organization;

import com.jwabetybo.dto.OrganizationRequest;
import com.jwabetybo.dto.OrganizationResponse;

import java.util.List;

public interface OrganizationService {
    List<OrganizationResponse> getAllOrganization();
    Long createOrganization(OrganizationRequest organizationRequest);
}
