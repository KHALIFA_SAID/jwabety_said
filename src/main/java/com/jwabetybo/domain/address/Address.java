package com.jwabetybo.domain.address;

import com.jwabetybo.domain.organization.Organization;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long num;
    private String street;
    @Column(name = "postal_code")
    private String postalCode;
    private String city;
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organisation;

}
