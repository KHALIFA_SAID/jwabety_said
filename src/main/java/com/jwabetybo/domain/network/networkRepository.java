package com.jwabetybo.domain.network;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface networkRepository extends JpaRepository<Network,Integer> {
}
