package com.jwabetybo.domain.organization;

import com.jwabetybo.OrganizationApiDelegate;
import com.jwabetybo.OrganizationsApiDelegate;
import com.jwabetybo.dto.OrganizationRequest;
import com.jwabetybo.dto.OrganizationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;
@Slf4j
@RestController
public class OrganizationController implements OrganizationApiDelegate, OrganizationsApiDelegate {

    private final OrganizationService organizationService;

    public OrganizationController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @Override
    public ResponseEntity<Long> organizationPost(OrganizationRequest organizationRequest) {
        return null;
    }

    @Override
    public ResponseEntity<List<OrganizationResponse>> organizationsGet() {
        organizationService.getAllOrganization();
        return null;
    }
}
